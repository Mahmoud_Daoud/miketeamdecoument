LOG-NODE

WaterValv:


[ReqId001v1]: User will have an User_Interface to connect the system.

[ReqId002v1]: User will be able to access the complete Log_File
[ReqId003v1]: Log will save the current status of water valve ( ON=1, OFF=0 ).
[ReqId004v1]: Log will save the current status of <Moisture_Level> (1-10).

[ReqId009v1]: Log will save the current status of temperature <Temp_Value>.
[ReqId013v1]: Log will save the current status of Lights ( ON=1, OFF=0 ).

[ReqId014v1]: Log will save the current status of intensity of color <Color_Intensity>.

[ReqId005v1]: Log will save the set value for minumum moisture <Moisture_Min>.

[ReqId006v1]: Log will save the set value for maximum Moisture <Moisture_Max>.

[ReqId010v1]: Log will save the the set value for windows minimum <WindowsVent_Min>.
[ReqId011v1]: Log will save the set value for windows maximum <WindowsVent_Max>.

[ReqId016v1]: Log will save the set value for color of light <color_Value>.

[ReqId020v1]: Log will save the set value for the minimum  temperature (Temp_Min).

[ReqId021v1]: Log will save the set value for the maximum  temperature (Temp_Max).

[ReqId025v1]: Log will save the set value for the fertilizer pump minimum (FertzPump_Min).

[ReqId026v1]: Log will save the set value for the fertilizer pump maximum (FertzPump_Max).


[ReqId007v1]: Log will save the failure report on the water valve <WaterValve_Failure>

[ReqId017v1]: Log will save the the failure report on lights <Lights_Failure>.
[ReqId022v1]: Log will save the failure report on heater failure <Heater_Failure>.

[ReqId027v1]: Log will save the failure report on fertilizer pump <FertzPump_Failure>.

[ReqId029v1]: Log will save the failure report on water pump <WaterPump_Failure>.


[ReqId031v1]: Log will save the failure report on Fans <FertzPump_Failure>.

[ReqId012v1]: Log will save the failure report on windows <WindowsVent_Failure>


[ReqId008v1]: Log will save the status of Windows Vent ( ON=1, OFF=0 ).

[ReqId015v1]: Log will save the status lights color <Color_Lights>.

[ReqId018v1]: Log will save the status of Heater ( ON=1, OFF=0 ).
[ReqId023v1]: Log will save the status of FertzPump ( ON=1, OFF=0 ).

[ReqId028v1]: Log will save the status of WaterPump ( ON=1, OFF=0 ).

[ReqId030v1]: Log will save the status of Fans ( ON=1, OFF=0 ).


[ReqId019v1]: Log will save the soil temperature <soil_Temp>.


[ReqId024v1]: Log will save the Fertilizer level <Fertz_Level>.

[ReqId032v1]: log will save all the values with timestamps.
